package devutil

import (
	"strconv"
	"testing"
)

func TestFExclusion(t *testing.T) {
	for key, v := range []struct {
		in   string
		want bool
	}{
		{"somefile.lOg", true},
		{"bad file . log", false},
		{"still _ good.log", true},
		{".log", true},
		{"file", false},
		{"file.txt", false},
		{".", true},
		{"..", true},
		{"bin", true},
		{"server", true},
		{"devserver", true},
		{"main.wasm", true},
		{"main.wasm.gzip", true},
	} {

		got := FExclusion(v.in)
		if got != v.want {
			t.Errorf("%v) FExclusion(%q) == %q, want %q", key, v.in, strconv.FormatBool(got), strconv.FormatBool(v.want))
		}
	}
}
