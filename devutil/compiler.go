// NOTE: https://webassembly.org/ says "Wasm" not "WASM" or "WAsm", so that's what I went with on the name.

package devutil

import (
	"bytes"
	"compress/gzip"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

// WasmCompiler provides a convenient way to call `go generate` and `go build` and produce Wasm executables for your system.
type WasmCompiler struct {
	sInDir, sOutDir                  string
	sAppBinFileName, sAppZipFileName string
	beforeFunc                       func() error
	generateCmdFunc                  func(path string) *exec.Cmd
	buildCmdFunc                     func(inpath, outpath string) *exec.Cmd
	afterFunc                        func(outpath string, err error) error
	le, li                           *log.Logger
	//logWriter                        io.Writer
}

// NewWasmCompiler returns a WasmCompiler instance.
func NewWasmCompiler() (c *WasmCompiler) {
	c = &WasmCompiler{
		//logWriter: os.Stderr,
		//le: *log.New(os.Stderr, "ERROR: ", log.Default().Flags()),
		//li: *log.New(os.Stdout, "INFO: ", log.Default().Flags()),
		le: log.Default(),
		li: log.Default(),
	}

	c.SetGenerateCmdFunc(func(path string) *exec.Cmd {
		cmd := exec.Command("go", "generate")
		cmd.Dir = path
		return cmd
	})

	c.SetBuildCmdFunc(func(inpath, outpath string) *exec.Cmd {
		cmd := exec.Command("go", "build", "-o", outpath)
		cmd.Dir = inpath
		cmd.Env = os.Environ()
		cmd.Env = append(cmd.Env, "GOOS=js", "GOARCH=wasm")
		return cmd
	})

	return c
}

func (c *WasmCompiler) SetInOutDirs(inpath, outpath string) *WasmCompiler {
	c.sInDir, c.sOutDir = inpath, outpath
	return c
}

func (c *WasmCompiler) SetOutputFiles(binFileName, zipFilename string) *WasmCompiler {
	c.sAppBinFileName, c.sAppZipFileName = binFileName, zipFilename
	return c
}

// SetLogWriter sets the writer to use for logging output.  Setting it to nil disables logging.
// The default from NewWasmCompiler is os.Stderr
/*func (c *WasmCompiler) SetLogWriter(w io.Writer) *WasmCompiler {
	if w == nil {
		w = io.Discard
	}
	//c.logWriter = w
	c.le.SetOutput(w)
	c.li.SetOutput(w)
	return c
}*/

// SetLoggers sets logger for Printing Errors and Info. The defaults from NewWasmCompiler are log.Default()
func (c *WasmCompiler) SetLoggers(logError, logInfo *log.Logger) *WasmCompiler {
	c.le = logError
	c.li = logInfo
	return c
}

// SetBuildCmdFunc provides a function to create the exec.Cmd used when running `go build`.
// It overrides any other build-related setting.
func (c *WasmCompiler) SetBuildCmdFunc(cmdf func(inpath, outpath string) *exec.Cmd) *WasmCompiler {
	c.buildCmdFunc = cmdf
	return c
}

// SetGenerateCmdFunc provides a function to create the exec.Cmd used when running `go generate`.
// It overrides any other generate-related setting.
func (c *WasmCompiler) SetGenerateCmdFunc(cmdf func(path string) *exec.Cmd) *WasmCompiler {
	c.generateCmdFunc = cmdf
	return c
}

// SetBeforeFunc specifies a function to be executed before anything else during Execute().
func (c *WasmCompiler) SetBeforeFunc(f func() error) *WasmCompiler {
	c.beforeFunc = f
	return c
}

// SetAfterFunc specifies a function to be executed after everthing else during Execute().
func (c *WasmCompiler) SetAfterFunc(f func(outpath string, err error) error) *WasmCompiler {
	c.afterFunc = f
	return c
}

func (c *WasmCompiler) logError(e error) error {
	if e == nil {
		return nil
	}
	//fmt.Fprintln(c.logWriter, e)
	c.le.Println(e)
	return e
}

// Execute runs the generate command (if any) and then invokes the Go compiler
// and produces a wasm executable (or an error).
// The value of outpath is the absolute path of the output files (*.wasm and *.gzip) on disk.
func (c *WasmCompiler) Execute() (outpath string, err error) {
	if c.buildCmdFunc == nil {
		return "", c.logError(errors.New("WasmCompiler: no build command set, cannot continue (did you call SetBulidDir?)"))
	}

	if c.beforeFunc != nil {
		err := c.beforeFunc()
		if err != nil {
			return "", c.logError(err)
		}
	}

	if c.generateCmdFunc != nil {
		cmd := c.generateCmdFunc(c.sInDir)
		b, err := cmd.CombinedOutput()
		if err != nil {
			return "", c.logError(fmt.Errorf("WasmCompiler: generate error: %w; full output:\n%s", err, b))
		}
		//fmt.Fprintln(c.logWriter, "WasmCompiler: Successful generate")
		c.li.Println("WasmCompiler: Successful generate")
	}

	cmd := c.buildCmdFunc(c.sInDir, filepath.Join(c.sOutDir, c.sAppBinFileName))
	b, err := cmd.CombinedOutput()
	if err != nil {
		return "", c.logError(fmt.Errorf("WasmCompiler: build error: %w; full output:\n%s", err, b))
	}
	//fmt.Fprintln(c.logWriter, "WasmCompiler: Successful build")
	c.li.Println("WasmCompiler: Successful build")

	if c.afterFunc != nil {
		err = c.afterFunc(c.sOutDir, err)
		if err != nil {
			return c.sOutDir, c.logError(err)
		}
	}

	err = c.packCmdFunc()
	if err != nil {
		return c.sOutDir, c.logError(err)
	}
	//fmt.Fprintln(c.logWriter, "WasmCompiler: Successful compressed")
	c.li.Println("WasmCompiler: Successful compressed")

	return c.sOutDir, nil
}

// Error in case file not found.
func (c *WasmCompiler) packCmdFunc() (err error) {
	fWasm, err := os.OpenFile(filepath.Join(c.sOutDir, c.sAppBinFileName), os.O_RDWR, 0666)
	if err != nil {
		return c.logError(fmt.Errorf("WasmCompiler: opening compiled file for packing error: %w\n", err))
	}
	defer fWasm.Close()

	fGZIP, err := os.OpenFile(filepath.Join(c.sOutDir, c.sAppZipFileName), os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		return c.logError(fmt.Errorf("WasmCompiler: creating GZIP file error: %w\n", err))
	}
	defer fGZIP.Close()

	// gzip with max compression
	var buf bytes.Buffer
	gzw, _ := gzip.NewWriterLevel(&buf, gzip.BestCompression)
	_, err = io.Copy(gzw, fWasm)
	if err != nil {
		return c.logError(fmt.Errorf("WasmCompiler: error reading compiled binary and compressing it: %w\n", err))
	}
	gzw.Close()

	//f.Truncate(0)
	//f.Seek(0, 0)
	_, err = io.Copy(fGZIP, &buf)
	if err != nil {
		return c.logError(fmt.Errorf("WasmCompiler: error saving compressed file: %w\n", err))
	}

	return nil
}

// WasmExecJS returns the contents of the wasm_exec.js file bundled with the Go compiler.
func (c *WasmCompiler) WasmExecJS() (r io.Reader, err error) {
	b1, err := exec.Command("go", "env", "GOROOT").CombinedOutput()
	if err != nil {
		return nil, err
	}

	b2, err := ioutil.ReadFile(filepath.Join(strings.TrimSpace(string(b1)), "misc/wasm/wasm_exec.js"))
	return bytes.NewReader(b2), err
}
