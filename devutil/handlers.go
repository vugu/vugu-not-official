package devutil

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"gitlab.com/al-coder/xservice"
)

type TWayOfCompile int

const (
	WayOfCompileNever       = iota // Must always feed main.wasm from provided to NewWasmCompiler() path.
	WayOfCompileUponRequest        // vugu.devutil.compiler.go default behavior (compile every time into temp file).
	WayOfCompileIfUpdated          // Now -  default behavior. Compile upon request if Timestamp was changed.
	// WayOfCompileIfUpdated doesn't make any sense if: devutil.NewWasmCompiler().SetCompileIntoTempFile(true)
)

// Compiler is implemented by WasmCompiler and TinygoCompiler.
type Compiler interface {
	Execute() (outpath string, err error)
}

// WasmExecJSer is implemented by WasmCompiler and TinygoCompiler.
type WasmExecJSer interface {
	WasmExecJS() (contents io.Reader, err error)
}

// MainWasmHandler calls WasmCompiler.Build and responds with the resulting .wasm file.
type MainWasmHandler struct {
	wayOfCompile                     TWayOfCompile
	wc                               Compiler
	sDir                             string
	dirTs                            time.Time
	sAppBinFileName, sAppZipFileName string
	le, li                           *log.Logger
}

// NewMainWasmHandler returns an initialized MainWasmHandler.
func NewMainWasmHandler(nwc Compiler) *MainWasmHandler {
	return &MainWasmHandler{
		wayOfCompile: WayOfCompileNever,
		wc:           nwc,
		dirTs:        time.Time{},
		le:           log.Default(),
		li:           log.Default(),
	}
}

// ServeHTTP implements http.Handler.
func (h *MainWasmHandler) SetWayOfCompile(woc TWayOfCompile) *MainWasmHandler {
	h.wayOfCompile = woc
	return h
}

func (h *MainWasmHandler) SetDir(dir string) *MainWasmHandler {
	h.sDir = dir
	return h
}

func (h *MainWasmHandler) SetOutputFiles(binFileName, zipFilename string) *MainWasmHandler {
	h.sAppBinFileName, h.sAppZipFileName = binFileName, zipFilename
	return h
}

// SetLoggers sets logger for Printing Errors and Info. The defaults from NewMainWasmHandler are log.Default()
func (h *MainWasmHandler) SetLoggers(logError, logInfo *log.Logger) *MainWasmHandler {
	h.le = logError
	h.li = logInfo
	return h
}

// ServeHTTP implements http.Handler.
func (h *MainWasmHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var outpath string
	var err error
	fBuildWasmFileName := func(fn string) string {
		return filepath.Join(outpath, fn)
	}
	switch h.wayOfCompile {
	case WayOfCompileNever:
		outpath = h.sDir
	case WayOfCompileUponRequest:
		outpath, err = h.wc.Execute()
		defer func() {
			_ = os.Remove(fBuildWasmFileName(h.sAppBinFileName))
			_ = os.Remove(fBuildWasmFileName(h.sAppZipFileName))
		}()
	default: //WayOfCompileIfUpdated
		outpath = h.sDir
		if h.codeWasUpdated(filepath.Dir(outpath)) || (err != nil) { //NB: Do not change order of the IF, as we need to setup h.dirTs in any case.
			outpath, err = h.wc.Execute()
		}
	}

	if err != nil {
		h.le.Printf("MainWasmHandler: Execute error: %v\n", err)
		w.Header().Set("Content-Type", "text/plain; charset=utf-8")
		http.Error(w, "MainWasmHandler: Execute error:\n"+err.Error(), 500)
		return
	}

	w.Header().Set("Content-Type", "application/wasm")

	//From simple-handler.go:
	// by default we tell browsers to always check back with us for content, even in production;
	// we allow disabling by the caller just setting another value first; otherwise too much
	// headache caused by pages that won't reload and we still reduce a lot of bandwidth usage with
	// 304 responses, seems like a sensible trade off for now
	if w.Header().Get("Cache-Control") == "" {
		w.Header().Set("Cache-Control", "max-age=0, no-cache")
	}

	// if client supports gzip response (the usual case), we just set the gzip header and send back
	gziped := strings.Contains(r.Header.Get("Accept-Encoding"), "gzip")
	var wasmFileName string
	if gziped {
		wasmFileName = fBuildWasmFileName(h.sAppZipFileName)
	} else {
		wasmFileName = fBuildWasmFileName(h.sAppBinFileName)
	}

	f, err := os.Open(wasmFileName)
	if err != nil {
		h.le.Printf("MainWasmHandler: File open error: %v\n", err)
		w.Header().Set("Content-Type", "text/plain; charset=utf-8")
		http.Error(w, "MainWasmHandler: File open error:\n"+err.Error(), 500)
		return
	}
	defer f.Close()

	st, err := f.Stat()
	if err != nil {
		h.le.Printf("MainWasmHandler: File stat error: %v\n", err)
		w.Header().Set("Content-Type", "text/plain; charset=utf-8")
		http.Error(w, "MainWasmHandler: File stat error:\n"+err.Error(), 500)
		return
	}

	if gziped {
		w.Header().Set("Content-Encoding", "gzip")
		w.Header().Set("X-Gzipped-Size", fmt.Sprint(st.Size()))
	}

	http.ServeContent(w, r, r.URL.Path, st.ModTime(), f)
}

// Returns true if Directory Element should be excluded from checking for update.
func fExclusion(elem string) bool {
	exclusionElementsList := xservice.NewStringList("", false)
	*exclusionElementsList = append([]string(*exclusionElementsList), ".", "..", "bin", "server", "devserver", "0_components_vgen.go")
	ell := strings.ToLower(elem)
	if exclusionElementsList.HasValue(ell) || ell[0] == '.' { //ignore hiden elements
		return true
	}
	if strings.HasSuffix(ell, ".log") || strings.HasSuffix(ell, ".sum") || strings.HasSuffix(ell, ".pem") || strings.HasSuffix(ell, ".wasm") || strings.HasSuffix(ell, ".gzip") {
		return true
	}
	return false
}

// Returns true if Client Side Code was updated.
func (h *MainWasmHandler) codeWasUpdated(dirName string) (result bool) {
	newDirTs, err := dirTimestamp(dirName, fExclusion)
	if err != nil {
		h.le.Printf("error in dirTimestamp(%q): %v\n", dirName, err)
		return true
	}
	if newDirTs.IsZero() {
		return true
	}
	if newDirTs.After(h.dirTs) {
		h.dirTs = newDirTs
		return true
	}
	return false
}

// WasmExecJSHandler calls WasmCompiler.WasmExecJS and responds with the resulting .js file.
// WasmCompiler.WasmExecJS will only be called the first time and subsequent times
// will return the same result from memory.  (We're going to assume that you'll restart
// whatever process this is running in when upgrading your Go version.)
type WasmExecJSHandler struct {
	wc WasmExecJSer

	rwmu    sync.RWMutex
	content []byte
	modTime time.Time
}

// NewWasmExecJSHandler returns an initialized WasmExecJSHandler.
func NewWasmExecJSHandler(wc WasmExecJSer) *WasmExecJSHandler {
	return &WasmExecJSHandler{
		wc: wc,
	}
}

// ServeHTTP implements http.Handler.
func (h *WasmExecJSHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	h.rwmu.RLock()
	content := h.content
	modTime := h.modTime
	h.rwmu.RUnlock()

	if content == nil {

		h.rwmu.Lock()
		defer h.rwmu.Unlock()

		rd, err := h.wc.WasmExecJS()
		if err != nil {
			log.Printf("error getting wasm_exec.js: %v\n", err)
			http.Error(w, "error getting wasm_exec.js: "+err.Error(), 500)
			return
		}

		b, err := ioutil.ReadAll(rd)
		if err != nil {
			log.Printf("error reading wasm_exec.js: %v\n", err)
			http.Error(w, "error reading wasm_exec.js: "+err.Error(), 500)
			return
		}

		h.content = b
		content = h.content
		h.modTime = time.Now()
		modTime = h.modTime

	}

	w.Header().Set("Content-Type", "text/javascript")
	http.ServeContent(w, r, r.URL.Path, modTime, bytes.NewReader(content))
}

// func from https://github.com/vugu/vugu/blob/v0.3.4/simplehttp/simple-handler.go
// dirTimestamp finds the most recent time stamp associated with files in a folder
// TODO: we should look into file watcher stuff, better performance for large trees
func dirTimestamp(dir string, fExclusion func(elem string) bool) (ts time.Time, reterr error) {
	const iReaddirNormalBehavior = -1

	dirf, err := os.Open(dir)
	if err != nil {
		return ts, err
	}
	defer dirf.Close()

	fis, err := dirf.Readdir(iReaddirNormalBehavior)
	if err != nil {
		return ts, err
	}

	for _, fi := range fis {
		//first exclude: hidden elements; bin, log files, etc.
		if fExclusion != nil {
			if fExclusion(fi.Name()) {
				continue
			}
		}

		// for directories we recurse
		if fi.IsDir() {
			dirTs, err := dirTimestamp(filepath.Join(dir, fi.Name()), fExclusion)
			if err != nil {
				return ts, err
			}
			if dirTs.After(ts) {
				ts = dirTs
			}
			continue
		}

		// for files check timestamp
		mt := fi.ModTime()
		if mt.After(ts) {
			ts = mt
		}
	}
	return
}
